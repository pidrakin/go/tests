package tests

import (
	"os"
	"testing"
)

func TestChTmpDir(t *testing.T) {
	oldWd, err := os.Getwd()
	if err != nil {
		t.Errorf("%v", err)
	}

	toDefer := ChTmpDir(t)

	wd, err := os.Getwd()
	if err != nil {
		t.Errorf("%v", err)
	}

	if oldWd == wd {
		t.Errorf("current working directory should be a temporary one")
	}

	toDefer()

	newWd, err := os.Getwd()
	if err != nil {
		t.Errorf("%v", err)
	}

	if oldWd != newWd {
		t.Errorf("current working directory should be the old working directory")
	}

	if _, err = os.Stat(wd); !os.IsNotExist(err) {
		t.Errorf("the temporary working directory should have been cleanedf up")
	}
}
