package tests

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

var (
	NoError    = "Expected no error, got: %v"
	EqualError = "Error should be: %v, got: %v"
	FileExists = "File %v should exist"
	DirExists  = "Directory %v should exist"
	FileAbsent = "File %v should not exist"
	DirAbsent  = "Dir %v should not exist"
)

func ChTmpDir(t *testing.T) func() {
	cwd, err := os.Getwd()
	if err != nil {
		t.Errorf("chtmpdir: %v", err)
	}
	d, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Errorf("chtmpdir: %v", err)
	}
	if err = os.Chdir(d); err != nil {
		t.Errorf("chtmpdir: %v", err)
	}
	return func() {
		defer func(path string) {
			if err = os.RemoveAll(path); err != nil {
				t.Errorf("chtmpdir: %v", err)
			}
		}(d)
		if err = os.Chdir(cwd); err != nil {
			t.Errorf("chtmpdir: %v", err)
		}
	}
}

func RunInTmp(t *testing.T, f func(t *testing.T, wd string)) {
	defer ChTmpDir(t)()

	wd, err := os.Getwd()
	AssertNoError(t, err)

	f(t, wd)
}

func AssertNoError(t *testing.T, err error) bool {
	return assert.NoErrorf(t, err, NoError, err)
}

func AssertEqualError(t *testing.T, err error, expectedErrorMsg string) bool {
	return assert.EqualErrorf(t, err, expectedErrorMsg, EqualError, expectedErrorMsg, err)
}

func AssertFileExists(t *testing.T, path string) bool {
	return assert.FileExistsf(t, path, FileExists, path)
}

func AssertDirExists(t *testing.T, path string) bool {
	return assert.DirExistsf(t, path, DirExists, path)
}

func AssertFileAbsent(t *testing.T, path string) bool {
	return assert.NoFileExistsf(t, path, FileAbsent, path)
}

func AssertDirAbsent(t *testing.T, path string) bool {
	return assert.NoDirExistsf(t, path, DirAbsent, path)
}
